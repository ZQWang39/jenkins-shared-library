#!/usr/bin/env groovy
package com.example

class Docker implements Serializable{
    def script
    Docker(script){
        this.script = script
    }
    def buildDockerImage(String imageName){
        script.echo "building docker image..."
        script.sh "docker build -t $imageName ."
    }
    def dockerLogin(){
        script.sh 'echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin'
    }
    def dockerPush(String imageName){
        script.sh "docker push $imageName"
    }
}
